//soal 1
var nilai = "99"
if (nilai >= 85){
    console.log("A")
}
else if(nilai >= 75) {
	console.log("B")
}
else if(nilai >= 65) {
	console.log("C")
} 
else if(nilai >= 55) {
	console.log("D")
} 
else{
	console.log("E")
}  

//soal 2
var tanggal = 17;
var bulan = 6;
var tahun = 1995;

switch(bulan) {
	case 1:   { console.log(tanggal+' Januari '+tahun); break; }
	case 2:   { console.log(tanggal+' Februari '+tahun); break; }
	case 3:   { console.log(tanggal+' Maret '+tahun); break; }
	case 4:   { console.log(tanggal+' April '+tahun); break; }
	case 5:   { console.log(tanggal+' Mei '+tahun); break; }
	case 6:   { console.log(tanggal+' Juni '+tahun); break; }
	case 7:   { console.log(tanggal+' Juli '+tahun); break; }
	case 8:   { console.log(tanggal+' Agustus '+tahun); break; }
	case 9:   { console.log(tanggal+' September '+tahun); break; }
	case 10:   { console.log(tanggal+' Oktober '+tahun); break; }
	case 11:   { console.log(tanggal+' November '+tahun); break; }
	case 12:   { console.log(tanggal+' Desember '+tahun); break; }
	default:  { console.log('Format tidak sesuai'); }
}


//soal 3
var n = 5;
let hasil = '';
for (let i = 0; i < n; i++) {
    for (let j = 0; j <= i; j++) {
        hasil += '* ';
    }
    hasil += '\n';
}
console.log(hasil)