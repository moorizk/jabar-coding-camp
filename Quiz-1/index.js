//Soal No. 1
function next_date(a, b, c) {
  a++
  var hari = 31
  if (b == 4 | b == 6 | b == 9 | b == 11) {
    hari = 30
  }
  else if(b == 2){
    hari = 28
    if(c % 4 == 0) {
      hari = 29
      if(c % 100 == 0) {
        hari = 28
        if(c % 400 == 0) {
          hari = 29
        }
      }
    }
  }
  if (a > hari) {
    a = 1
    b++
    if (b > 12) {
      b = 1
      c++
    }
  }
  var bulan
  switch(b){
  	case 1:bulan="Januari"; break;
    case 2:bulan="Februari"; break;
    case 3:bulan="Maret"; break;
    case 4:bulan="April"; break;
    case 5:bulan="Mei"; break;
    case 6:bulan="Juni"; break;
    case 7:bulan="Juli"; break;
    case 8:bulan="Agustus"; break;
    case 9:bulan="September"; break;
    case 10:bulan="Oktober"; break;
    case 11:bulan="November"; break;
    case 12:bulan="Desember"; break;
  }
  var hasil = a+" "+bulan+" "+c
  return hasil
}

var tanggal = 29
var bulan = 2
var tahun = 2020

var data = next_date(tanggal , bulan , tahun)
console.log(data)

//Soal No. 2
function jumlah_kata(data) {
	var jml = 0

    // state:
    for(var i=0; i<data.length-1; i++){
   		if(data[i] == ' ' & data[i+1] != ' '){
			jml++
		}
   }
	return jml
}

var kalimat_1 = " Halo nama saya Muhammad Iqbal Mubarok "
var kalimat_2 = " Saya Iqbal"
var kalimat_3 = " Saya Muhammad Iqbal Mubarok "

console.log(jumlah_kata(kalimat_1))
console.log(jumlah_kata(kalimat_2))
console.log(jumlah_kata(kalimat_3))