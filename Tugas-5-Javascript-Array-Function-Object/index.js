//soal 1
var daftarHewan = ["2. Komodo", "5. Buaya", "3. Cicak", "4. Ular", "1. Tokek"];
daftarHewan.sort()
console.log(daftarHewan)

//soal 2
function introduce(param) {
  var kata = "Nama saya "+param['name']+", umur saya "+param['age']+" tahun, alamat saya di "+param['address']+", dan saya punya hobby yaitu "+param['hobby']
  return kata
}
 
var data = {name : "John" , age : 30 , address : "Jalan Pelesiran" , hobby : "Gaming" }
 
var perkenalan = introduce(data)
console.log(perkenalan) // Menampilkan "Nama saya John, umur saya 30 tahun, alamat saya di Jalan Pelesiran, dan saya punya hobby yaitu Gaming"

//soal 3
function hitung_huruf_vokal(data){
   var jml=0;
   for(var i=0; i<data.length; i++){
   		if(data[i] == 'a' | data[i] == 'b' | data[i] == 'c' | data[i] == 'd' | data[i] == 'e'){
        	jml++
        }
   }
   return jml
}

var hitung_1 = hitung_huruf_vokal("Muhammad")
var hitung_2 = hitung_huruf_vokal("Iqbal")
console.log(hitung_1 , hitung_2) // 3 2