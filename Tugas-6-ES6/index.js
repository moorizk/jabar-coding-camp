//soal 1
const luas = (p, l) => p * l;
const keliling = (p, l) => 2*p + 2*l;
console.log(luas(5, 2));
console.log(keliling(5, 2));

//soal 2
const newFunction = function literal(firstName, lastName){
  return {
    firstName: firstName,
    lastName: lastName,
    fullName: function(){
      console.log(firstName + " " + lastName)
    }
  }
}
 
//Driver Code 
newFunction("William", "Imoh").fullName() 

//Soal 3
const newObject = {
  firstName: "Muhammad",
  lastName: "Iqbal Mubarok",
  address: "Jalan Ranamanyar",
  hobby: "playing football",
}
const {firstName, lastName, address, hobby} = newObject
console.log(firstName, lastName, address, hobby)

//Soal 4
const west = ["Will", "Chris", "Sam", "Holly"]
const east = ["Gill", "Brian", "Noel", "Maggie"]
let combined = [...west, ...east]
//Driver Code
console.log(combined)

//Soal 5
const planet = "earth" 
const view = "glass" 
//var before = `Lorem ${view} dolor sit amet, consectetur adipiscing elit, ${planet)
var before = (`Lorem ${view} dolor sit amet, consectetur adipiscing elit, not ${planet}`);
console.log(before)
